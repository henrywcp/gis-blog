# Table of Contents

* [Overview](./2_stamps_workflow.md)
* [Region of Interest](./2-1_roi.md)
* [Get SENTINEL-1 data](#2-get-sentinel-1-data)
* [snap2stamps](./2-3_snap2stamps.md)
* [StaMPS](./2-4_StaMPS-steps.md)
* [StaMPS Visualizer](./2-5_shiny.md)

# 2 Get SENTINEL-1 data
# 2.1 Download through Open Access Hub
Downloading a bulk of Sentinel images, either S1 or S2, is not very smooth if you use the GUI from the Open Access Hub.
But if you have an account, you can use the `sentinelsat` module from your python interpreter.

```python
from sentinelsat import SentinelAPI, read_geojson, geojson_to_wkt
api = SentinelAPI('username', 'password')
footprint = geojson_to_wkt(read_geojson('/path/to/roi.geojson'))
products = api.query(footprint,
                     platformname='Sentinel-1',
                     producttype='SLC',
                     orbitdirection='DESCENDING',
                     beginposition='[YYYY-MM-DDT00:00:00.000Z TO YYYY-MM-DDT00:00:00.000Z]',
                     sensoroperationalmode='IW',
                     polarisationmode='VV')
api.download_all(products, directory_path='/path/to/directory')
```

To see all options you can use to communicate with the server have a look at the
[sentinelsat documentation](https://sentinelsat.readthedocs.io/en/stable/api.html)
and the
[original API documentation of the data hub](https://scihub.copernicus.eu/twiki/do/view/SciHubUserGuide/FullTextSearch?redirectedfrom=SciHubUserGuide.3FullTextSearch).

## 2.2 Alaska satellite facility
This option uses python scripts for Downloading data as well. However, it is usually faster than the `sentinelsat` approach.
All information and instructions can ve found at <https://www.asf.alaska.edu/data-tools/bulk-download/>.

## 2.3 EODC
In order to avoid downloading a potentially large amount of files, data stored
at the EODC server can be used:  
```python
import os
from sentinelsat import SentinelAPI, read_geojson, geojson_to_wkt
from datetime import date
import shutil

path_output = '/home/username/.../zip_files/'
eodc_path = '/eodc/products/copernicus.eu/s1b_csar_slc__iw/'
dates = ('YYYYMMDD', 'YYYYMMDD')
api = SentinelAPI('username', 'password', 'https://scihub.copernicus.eu/dhus')

# search by polygon, time, and SciHub query keywords
footprint = geojson_to_wkt(read_geojson('/home/username/.../roi.geojson'))

products = api.query(footprint, dates, platformname='Sentinel-1', producttype='SLC').values()

for p in products:
    p_name = p['title']
    p_orbit = p['relativeorbitnumber']
    # obtain orbit from Copernicus Open Access Hub
    if p_orbit == 146:
        # only S1B
        if p_name.startswith('S1B'):
            infile=eodc_path + p_name[17:21] + '/' + p_name[21:23]+ '/' + p_name[23:25] + '/' + p_name + '.zip'
            print(infile)
            outfile = path_output + p_name + '.zip'
            if os.path.exists(infile):                
                shutil.copyfile(infile,outfile)
                print('copy...' + p_name)
```

