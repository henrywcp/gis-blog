# Table of Contents

* [Background](#background)
* [Reguired data](#required-data)
* [Conversion](#conversion)

# Background

Convert line of sight measurements to vertical (up/down) and horizontal (E-W) displacements.
See e.g. [Delgado Blasco et al., (2019)](https://doi.org/10.3390/rs11020129) for reference.

---

## Required data

### StaMPS export

### Incidence Angle

Steps in SNAP in order to extract Incidence Angle information:

- Radar &rarr; Sentinel-1 TOPS &rarr; S-1 TOPS Deburst
- Radar &rarr; Geometric &rarr; Terrain Correction &rarr; Range-Doppler Terrain Correction
- Select Incidence angle from ellipsoid (Pixel Spacing 5m)
- Raster &rarr; Subset

## Conversion

### PSI filtering

wip, tbd

### Nearest neightbor matching of PSI points

wip, tbd

