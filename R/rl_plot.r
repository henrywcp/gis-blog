# basic plotting function ----
rlplot <- function(GEV_MLE = NULL, GEV_LMOM = NULL,
                   GP_MLE = NULL, GP_LMOM = NULL,
                   GEV_GMLE = NULL, GP_GMLE = NULL,
                   GEV_BPE = NULL, GP_BPE = NULL,
                   rperiods = NULL, do_ci = TRUE,
                   period = "years", unit = "",
                   ylab = "Return Level",
                   y_seq = seq(0, 200, 25), ylim = NULL,
                   AMS_pts = TRUE, PDS_pts = TRUE,
                   colors = NULL, plottitle = "Return Level Plot"){
  if(is.null(colors)){
    colors <- c("#1b9e77", "#d95f02", "#7570b3", "#e7298a",
                "#66a61e", "#e6ab02", "#a6761d" ,"#666666")
  }
  if(is.null(ylim)){
    ylim <- c(min(y_seq), max(y_seq))
  }
  if (is.null(rperiods)){
    rperiods <- c(1+1e-15, 1.25, 1.5, 2, 3,4,5, 7.5, 10, 15, 20,
                  30, 40, 50, 60, 75, 80, 100, 120, 200, 250)
  }
  ylab <- trimws(paste(ylab, unit))
  
  # empty plot
  plot(0, type="n",
       xlab=paste0("Return Period [", period, "]"),
       ylab=ylab,
       xaxt='n',yaxt='n', log = 'x',
       xlim=c(min(rperiods), max(rperiods)),
       ylim=ylim,
       main = plottitle)
  # axis
  par(las = 1)
  x_ticks <- c(1,2,5,10,20,50,100,200)
  axis(side = 1, at = x_ticks)
  axis(side = 2, at = y_seq)
  # grid lines
  abline(v=x_ticks,col="darkgrey",lty=2)
  abline(h=y_seq,col="darkgrey",lty=2)
  
  legend_entries <- vector()
  legend_colors <- vector()
  
  data_vec <- c("GEV_MLE", "GEV_LMOM", "GEV_GMLE", "GP_GMLE",
                "GP_MLE", "GP_LMOM", "GEV_BPE", "GP_BPE")
  legend_vec <- paste(rep(c("GEV", "GP"), each = 4), "/w",
                      rep(c("MLE", "LMOM", "GMLE", "BPE"),2))
  for(i in seq_along(data_vec)){
    tmp <- get(data_vec[i])
    m <- tmp$method
    if(m %in% c("MLE", "GMLE")){
      rlplot_mle(tmp, rperiods = rperiods, do_ci = do_ci, colors = colors)
    }
    if(m == "Lmoments"){
      rlplot_lmom(tmp, rperiods = rperiods, do_ci = do_ci, colors = colors)
    }
    if(m == "Bayesian"){
      rlplot_bayes(tmp, rperiods = rperiods, do_ci = do_ci, colors = colors)
    }
    legend_entries <- c(legend_entries, legend_vec[i])
    legend_colors <- c(legend_colors, colors[i])
  }
  
  # legend
  lwd_vec <- rep(2, length(legend_entries))
  lty_vec <- rep(1, length(legend_entries))
  pch_vec <- rep(NA, length(legend_entries))
  if(AMS_pts){
    lwd_vec <- c(lwd_vec, NA)
    lty_vec <- c(lty_vec, NA)
    pch_vec <- c(pch_vec, 21)
    legend_entries = c(legend_entries, "AMS")
    legend_colors = c(legend_colors, "black")
  }
  if(PDS_pts){
    lwd_vec <- c(lwd_vec, NA)
    lty_vec <- c(lty_vec, NA)
    pch_vec <- c(pch_vec, 3)
    legend_entries = c(legend_entries, "PDS")
    legend_colors = c(legend_colors, "dimgrey")
  }
  legend("topleft", legend = legend_entries, col = legend_colors, 
         bg = "white", lwd = lwd_vec, lty = lty_vec, pch = pch_vec)
}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# MLE ---
rlplot_mle <- function (x, rperiods, period, unit, y_seq, ylim, do_ci,
                        prange = NULL, d = NULL, colors) 
{
  model <- x$type
  method <- x$method
  pars <- x$results$par
  ytmp <- datagrabber(x)
  if (x$data.name[2] != ""){
    data <- ytmp[, -1]
  } else {
    data <- NULL
  }
  y <- c(ytmp[, 1])
  const.thresh <- check.constant(x$par.models$threshold)
  const.loc <- check.constant(x$par.models$location)
  const.scale <- check.constant(x$par.models$scale)
  const.shape <- check.constant(x$par.models$shape)
  tform <- !is.fixedfevd(x)
  if (tform) {
    ytrans <- trans(x)
  }
  if (!tform) {
    if (!is.element(model, c("GP", "Beta", "Pareto", "Exponential"))) 
      loc <- pars["location"]
    else loc <- NULL
    if (is.element("scale", names(pars))) 
      scale <- pars["scale"]
    else scale <- exp(pars["log.scale"])
    if (!is.element(model, c("Gumbel", "Exponential"))) 
      shape <- pars["shape"]
    else shape <- 0
  }
  npy <- x$npy
  if (is.element(model, c("PP", "GP", "Beta", "Pareto", "Exponential"))){
    u <- x$threshold
    eid <- y > u
    lam <- mean(eid)
  } else {
    u <- lam <- NULL 
  }
  if (is.element(model, c("GP", "PP", "Beta", "Pareto", "Exponential"))){
    if (!tform){
      n <- sum(y > x$threshold)
    } else {
      n <- sum(!is.na(ytrans) & !is.nan(ytrans))
    }
  } else {
    n <- x$n
  } 
  xp <- ppoints(n = n, a = 0)
  # return level plot
  if (is.null(x$blocks)) {
    if (!(is.element(model, c("PP", "GP", "Exponential", "Beta", "Pareto")) &&
          !const.thresh && all(c(const.loc, const.scale, const.shape)))) {
      if (model == "PP"){
        mod2 <- "GEV"
        blocks <- rep(1:x$span, each = x$npy)
        n2 <- length(blocks)
        if (n2 < x$n) 
          blocks <- c(blocks, rep(blocks[n2], x$n - n2))
        else if (n2 > x$n) 
          blocks <- blocks[1:x$n]
        yEmp <- c(aggregate(y, by = list(blocks), max)$x)
      } else {
        mod2 <- model
      }
      if (!tform){
        yrl <- rlevd(rperiods, loc = loc, scale = scale, 
                     shape = shape, threshold = u, type = mod2, 
                     npy = npy, rate = lam)
        bds <- ci(x, return.period = rperiods)
        if(is.element(model, c("PP", "GEV", "Gumbel", "Weibull", "Frechet"))){
          xrl <- -1/(log(1 - 1/rperiods))
        } else {
          xrl <- rperiods
        }
        # MLE estimation lines
        if(method == "MLE"){
          if (model == "GEV"){
            lines(xrl, yrl, col = colors[1], lwd = 2)
            if(do_ci){
              lines(xrl, bds[, 1], col = colors[1], lty = 4)
              lines(xrl, bds[, 3], col = colors[1], lty = 4)
            }
          }
          if (model == "GP"){
            lines(xrl, yrl, col = colors[5], lwd = 2)
            if(do_ci){
              lines(xrl, bds[, 1], col = colors[5], lty = 4)
              lines(xrl, bds[, 3], col = colors[5], lty = 4)
            }
          }
        }
        # GMLE estimation lines
        if(method == "GMLE"){
          if (model == "GEV"){
            lines(xrl, yrl, col = colors[3], lwd = 2)
            if(do_ci){
              lines(xrl, bds[, 1], col = colors[3], lty = 4)
              lines(xrl, bds[, 3], col = colors[3], lty = 4)
            }
          }
          if (model == "GP"){
            lines(xrl, yrl, col = colors[7], lwd = 2)
            if(do_ci){
              lines(xrl, bds[, 1], col = colors[7], lty = 4)
              lines(xrl, bds[, 3], col = colors[7], lty = 4)
            }
          }
        }
        # points
        if (is.element(model, c("GEV", "Gumbel", "Weibull", "Frechet"))) 
          points(-1/log(xp), sort(y), pch=21, col="black")
        else if (is.element(model, c("GP", "Beta", "Pareto", "Exponential"))) {
          n2 <- x$n
          xp2 <- ppoints(n2, a = 0)
          sdat <- sort(y)
          points(-1/log(xp2)[sdat>u]/npy, sdat[sdat>u], pch="+", col="dimgrey")
        }
      } else {
        np <- length(rperiods)
        effrl <- matrix(NA, length(y), np)
        for (i in 1:np) effrl[, i] <- erlevd(x = x, period = rperiods[i])
        for (i in 1:np) lines(effrl[, i], lty = i, col = i + 1)
      }
    }
  }
  invisible()
}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# lmom ----
rlplot_lmom <- function (x, rperiods, do_ci, colors, d = NULL){
  model <- x$type
  p <- x$results
  pnames <- names(p)
  loc <- ifelse(is.element("location", pnames), p["location"], 0)
  scale <- p["scale"]
  loc <- ifelse(is.element("shape", pnames), p["shape"], 0)
  u <- ifelse(!is.null(x$threshold), x$threshold, 0)
  y <- datagrabber(x, cov.data = FALSE)
  if (is.element(model, c("PP", "GP", "Exponential", "Beta", "Pareto"))){
    eid <- y > u
  } else{
    eid <- !logical(x$n)
  }
  m <- sum(eid)
  xp <- ppoints(m, a = 0)
  plothelper_lmom(x = x, xp = xp, y = y, u = u, eid = eid, rperiods = rperiods,
                  do_ci = do_ci, colors = colors, model = model)
  invisible()
}

# plothelper lmom ----
plothelper_lmom <- function (x, xp, y, u, eid, rperiods, do_ci, colors,
                             model = c("GEV", "GP", "PP", "Gumbel", "Frechet",
                                       "Weibull", "Exponential", "Beta", "Pareto")) 
{
  mod2 <- model
  tform <- !is.fixedfevd(x)
  if (!tform) {
    bds <- ci(x, return.period = rperiods)
    yrl <- bds[, 2]
    if (is.element(model, c("PP", "GEV", "Gumbel", "Weibull", "Frechet"))){
      xrl <- -1/(log(1 - 1/rperiods))
    } else {
      xrl <- rperiods
    }
    # lmom estimation lines
    if (model == "GEV"){
      lines(xrl, yrl, col = colors[2], lwd = 2)
    } else {
      lines(xrl, yrl, col = colors[6], lwd = 2)
    }
    # CI lines
    if(do_ci){
      if (model == "GEV"){
        lines(xrl, bds[, 1], col = colors[2], lty = 4)
        lines(xrl, bds[, 3], col = colors[2], lty = 4)
      } else {
        lines(xrl, bds[, 1], col = colors[6], lty = 4)
        lines(xrl, bds[, 3], col = colors[6], lty = 4)
      }
    }
    # points
    if (is.element(model, c("GEV", "Gumbel", "Weibull", "Frechet"))) 
      points(-1/log(xp), sort(y), pch=21, col="black")
    else if (is.element(model, c("GP", "Beta", "Pareto", "Exponential"))) {
      n2 <- x$n
      xp2 <- ppoints(n2, a = 0)
      sdat <- sort(y)
      points(-1/log(xp2)[sdat > u]/x$npy, sdat[sdat>u], pch="+", col="dimgrey")
    }
  }
  invisible()
}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# bayesian ----
rlplot_bayes <- function (x, rperiods, do_ci, colors, burn.in = 499, d = NULL){
  model <- x$type
  p <- p2 <- x$results
  np <- ncol(p) - 1
  iters <- nrow(p)
  p <- p2 <- p[, 1:np]
  pnames <- colnames(p)
  if (is.element("log.scale", pnames)) {
    id <- pnames == "log.scale"
    p[, id] <- exp(p[, id])
    pnames[id] <- "scale"
    colnames(p) <- pnames
  }
  p <- apply(p[-(1:burn.in), ], 2, mean, na.rm = TRUE)
  tform <- !is.fixedfevd(x)
  if (!tform) {
    if (is.element(model, c("PP", "GEV", "Gumbel", "Weibull", "Frechet"))) {
      loc <- p["location"]
      nloc <- 1
    } else {
      loc <- nloc <- 0
    }
    scale <- p["scale"]
    nsc <- 1
    if (!is.element(model, c("Gumbel", "Exponential"))) {
      shape <- p["shape"]
      nsh <- 1
    }
    else {
      nsh <- shape <- 0
    }
    ytrans <- NULL
  } else {
    ytrans <- trans(x)
    if (model == "PP") 
      ytransPP <- trans(x, return.all = TRUE)
    designs <- setup.design(x)
    if (is.element(model, c("PP", "GEV", "Gumbel", "Weibull", "Frechet"))) {
      X.loc <- designs$X.loc
      nloc <- ncol(X.loc)
      loc <- rowSums(matrix(p[1:nloc], x$n, nloc, byrow = TRUE) * X.loc)
    } else {
      loc <- 0
      nloc <- 0
    }
    X.sc <- designs$X.sc
    nsc <- ncol(X.sc)
    scale <- rowSums(matrix(p[(nloc + 1):(nloc + nsc)], x$n, 
                            nsc, byrow = TRUE) * X.sc)
    if (x$par.models$log.scale) 
      scale <- exp(scale)
    if (!is.element(model, c("Gumbel", "Exponential"))) {
      X.sh <- designs$X.sh
      nsh <- ncol(X.sh)
      shape <- rowSums(matrix(p[(nloc + nsc + 1):np], x$n, 
                              nsh, byrow = TRUE) * X.sh)
    } else {
      shape <- 0
      nsh <- 0
    }
  }
  u <- ifelse(!is.null(x$threshold), x$threshold, 0)
  y <- datagrabber(x, cov.data = FALSE)
  if (model == "PP") {
    blocks <- rep(1:round(x$span), each = round(x$npy))
    n2 <- length(blocks)
    if (n2 < x$n) 
      blocks <- c(blocks, rep(blocks[n2], x$n - n2))
    else if (n2 > x$n) 
      blocks <- blocks[1:x$n]
    ybm <- c(aggregate(y, by = list(blocks), max)$x)
  }
  if (is.element(model, c("PP", "GP", "Exponential", "Beta", "Pareto"))){
    eid <- y > u
  } else {
    eid <- !logical(x$n)
  }
  m <- sum(eid)
  xp <- ppoints(m, a = 0)
  do.rlplot <- TRUE
  if (is.element(model, c("PP", "GP", "Exponential", "Beta", "Pareto"))) {
    const.thresh <- check.constant(x$par.models$threshold)
    const.loc <- check.constant(x$par.models$location)
    const.scale <- check.constant(x$par.models$scale)
    const.shape <- check.constant(x$par.models$shape)
    if (!const.thresh && all(c(const.loc, const.scale, const.shape))) 
      do.rlplot <- FALSE
    if (!do.rlplot) 
      stop("plot.fevd: invalid type argument for POT models
           with varying thresholds but constant parameters
           (are you sure you about this model choice?)")
  }
  if (do.rlplot) 
    out <- plothelper_bayes(x = x, xp = xp, y = y, u = u, eid = eid, 
                            rperiods = rperiods, tform = tform, model = model, 
                            do_ci = do_ci, colors = colors)
  invisible(out)
}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

# plothelper bayes ----
plothelper_bayes <- function (x, xp, y, u, eid, rperiods, tform = FALSE,
                              model = c("GEV", "GP", "PP", "Gumbel", "Frechet",
                                        "Weibull", "Exponential", "Beta", "Pareto"),
                              do_ci, colors) 
{
  if (x$method != "Lmoments") {
    const.thresh <- check.constant(x$par.models$threshold)
    const.loc <- check.constant(x$par.models$location)
    const.scale <- check.constant(x$par.models$scale)
    const.shape <- check.constant(x$par.models$shape)
    if (is.element(model, c("PP", "GP", "Exponential", "Beta", "Pareto")) &&
        !const.thresh && all(c(const.loc, const.scale, const.shape))) 
      stop("rlplot.evd: invalid type argument for POT models
           with varying thresholds but constant parameters
           (are you sure you about this model choice?)")
  }
  if (model == "PP") {
    if (!tform) 
      mod2 <- "GEV"
    else mod2 <- "GEV"
    blocks <- rep(1:x$span, each = x$npy)
    n2 <- length(blocks)
    if (n2 < x$n) 
      blocks <- c(blocks, rep(blocks[n2], x$n - n2))
    else if (n2 > x$n) 
      blocks <- blocks[1:x$n]
    yEmp <- c(aggregate(y, by = list(blocks), max)$x)
  } else {
    mod2 <- model
  }
  if (!tform) {
    bds <- ci(x, return.period = rperiods)
    yrl <- bds[, 2]
    if (is.element(model, c("PP", "GEV", "Gumbel", "Weibull", "Frechet"))) 
      xrl <- -1/(log(1 - 1/rperiods))
    else xrl <- rperiods
    # bayesian estimation lines
    if (model == "GEV"){
      lines(xrl, yrl, col = colors[4], lwd = 2)
    } else {
      lines(xrl, yrl, col = colors[8], lwd = 2)
    }
    # CI lines
    if(do_ci){
      if (model == "GEV"){
        lines(xrl, bds[, 1], col = colors[4], lty = 4)
        lines(xrl, bds[, 3], col = colors[4], lty = 4)
      } else {
        lines(xrl, bds[, 1], col = colors[8], lty = 4)
        lines(xrl, bds[, 3], col = colors[8], lty = 4)
      }
    }
    # points
    if (is.element(model, c("GEV", "Gumbel", "Weibull", "Frechet"))) 
      points(-1/log(xp), sort(y), pch=21, col="black")
    else if (is.element(model, c("GP", "Beta", "Pareto","Exponential"))) {
      n2 <- x$n
      xp2 <- ppoints(n2, a = 0)
      sdat <- sort(y)
      points(-1/log(xp2)[sdat > u]/x$npy, sdat[sdat > u],pch="+",col="dimgrey")
      out <- list(model = bds,
                  empirical = data.frame(transformed.points = -1/log(xp2)[sdat > u]/x$npy,
                                         sorted.level = sdat[sdat > u]))
    }
    else if (model == "PP") {
      if (is.null(a)) 
        xp2 <- ppoints(length(yEmp))
      else xp2 <- ppoints(length(yEmp), a = 0)
      points(-1/log(xp2), sort(yEmp))
      out <- list(model = bds, empirical = data.frame(transformed.points = -1/log(xp2), 
                                                      sorted.level = sort(yEmp)))
    }
  } else {
    np <- length(rperiods)
    n <- length(y)
    effrl <- matrix(NA, n, np)
    for (i in 1:np) effrl[, i] <- return.level(x, return.period = rperiods[i])
    for (i in 1:np) lines(effrl[, i], lty = i, col = i + 1)
    out <- list(series = y[eid], level = effrl)
  }
  invisible()
}
